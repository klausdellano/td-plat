extends Area2D

signal damage

func _ready():
	pass


func _on_TP_body_entered(body):
	if body.has_method("damage"):
		body.queue_free()
		emit_signal("damage")
