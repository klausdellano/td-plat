extends Node2D

onready var pre_enemy = preload("res://Scenes/Enemies.tscn")

export var start_time = 0

func _ready():
	randomize()
	$Timer.wait_time = start_time


func _on_Timer_timeout():
	var enemy = pre_enemy.instance()
	get_parent().add_child(enemy)
	enemy.position = $Pos.global_position
	$Timer.wait_time = rand_range(8,20)
	$Timer.start()
