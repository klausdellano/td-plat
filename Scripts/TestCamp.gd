extends Node

onready var TP = $TPs/TP
onready var TP2 = $TPs/TP2

export var life = 10

func _ready():
	TP.connect("damage", self, "on_damage_game")
	TP2.connect("damage", self, "on_damage_game")

func on_damage_game():
	life -= 1
	print("Game Life: ",life)
