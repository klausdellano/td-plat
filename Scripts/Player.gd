extends KinematicBody2D

var pre_tower = preload("res://Scenes/Tower.tscn")
var pre_bullet_pistol = preload("res://Scenes/Bullet_Pistol.tscn")

const FLOOR = Vector2(0,-1)
const SPEED = 300
const GRAVITY = 10
const JUMP_FORCE = -650

var velocity = Vector2()

var walk = false

var right
var left
var gun
var interact_normal
var interact_release

var press_tower = 0

var stair = false
var grav = true
var bullet_dir = "right"
var prev_bullet
var prev_pistol = 0
var anim = false

var new_anim = ""
var animation = ""

func _physics_process(delta):
	
	right = Input.is_action_pressed("ui_right")
	left = Input.is_action_pressed("ui_left")
	interact_normal = Input.is_action_pressed("Interact")
	interact_release = Input.is_action_just_released("Interact")
	gun = Input.is_action_pressed("Shoot")
	
	if grav:
		velocity.y += GRAVITY
	
	if left:
		walking(false)
		bullet_dir = "left"
		$Sprite.flip_h =  true
	elif right:
		walking(true)
		bullet_dir = "right"
		$Sprite.flip_h =  false
	else:
		velocity.x = 0
		walk = false
	
	if is_on_floor():
		if Input.is_action_pressed("ui_up"):
			print("up")
			
		if walk:
			new_anim = "walk"
		else:
			new_anim = "idle"
		
		if interact_normal:
			if press_tower >= 1:
				#if not anim:
				tower_const("atention")
			else:
				if not anim:
					tower_const("start")
				press_tower += delta
		
		if interact_release:
			stop_tower_const()
			if press_tower >= 1: 
				var tower = pre_tower.instance()
				get_parent().add_child(tower)
				tower.global_position = $".".global_position + Vector2(10,0)
				press_tower = 0
			else:
				press_tower = 0
		
		if gun and not prev_bullet and prev_pistol <= 0:
			var bullet = pre_bullet_pistol.instance()
			get_parent().add_child(bullet)
			bullet.position = self.position
			bullet.direction(bullet_dir)
			prev_pistol = .85
		prev_bullet = gun
		prev_pistol -= delta
		
	else:
		#jumpping
		pass
	
	if animation != new_anim:
		$Anim.play(new_anim)
		animation = new_anim
		
	
	velocity = move_and_slide(velocity, FLOOR)
	

func walking(value):
	if value:
		velocity.x = SPEED
		walk = true
	else:
		velocity.x = -SPEED
		walk = true

func super_jump():
	velocity.y += JUMP_FORCE


func tower_const(value):
	anim = true
	$Sprite_Icon.visible = true
	if value == "start":
		$Anim_Icon.play("start")
	elif value == "atention":
		$Anim_Icon.play("atention")
	yield(get_node("Anim_Icon"),"animation_finished")
	anim = false

func stop_tower_const():
	anim = false
	$Sprite_Icon.visible = false













