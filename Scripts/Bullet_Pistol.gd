extends Area2D


var dir = Vector2(1,0)
var vel = 400
var dam = 1
var life_time = .6


func _ready():
	pass

func _process(delta):
	translate(dir * vel * delta)
	life_time -= delta
	if life_time <= 0:
		queue_free()
	

func direction(value):
	if value == "left":
		dir = Vector2(-1,0)
	if value == "right":
		dir = Vector2(1,0)


func _on_Bullet_Pistol_body_entered(body):
	if body.has_method("damage"):
		body.damage(dam)
		queue_free()
