extends KinematicBody2D

var enemy = self

const FLOOR = Vector2(0,-1)
const SPEED = 50
const GRAVITY = 10
#const JUMP_FORCE = -300

#onready var target = get_parent().get_node("Player")

var velocity = Vector2()

var life = 3

var dam = 1
var body
var time = 0

func _ready():
	add_to_group("enemie")

func _physics_process(delta):
	
	if $Ray.is_colliding():
		if time <= 0:
			body = $Ray.get_collider()
			if body.has_method("damage"):
				body.damage(dam)
				time = 1
		else:
			time -= delta
	
	
	velocity.y += GRAVITY
	velocity.x = SPEED
	
	#var direction = Vector2((target.position.x - enemy.position.x),velocity.y)
	velocity = move_and_slide(velocity, FLOOR)

func damage(value):
	life -= value
	if life <= 0:
		queue_free()


